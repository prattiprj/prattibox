﻿using System;
using System.Threading;

namespace server
{
    class Program
    {
        private static string ServerIpAddress= "127.0.0.1";

        [STAThread]
        static void Main(string[] args)
        {   
            FileManager fm = new FileManager();

            Thread stopping_thread = new Thread(() =>
            {
                Console.WriteLine("Server is UP, press any key to exit");
                Console.ReadLine();
            });
            stopping_thread.Start();
            stopping_thread.Join();

            fm.Close();
         }   

    }   
}
