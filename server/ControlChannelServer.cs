﻿using message_protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace server
{
    public delegate void OnLoginHandler(string Username, int connID);
    public delegate void OnMessageReceivedHandler(Message m, int connID);
    class ControlChannelServer
    {
        struct ClientInfo
        {
            public Socket socket;   //Socket of the client
            public string strName;  //Name by which the user logged into the chat room
            public int connID;
        }
        //The collection of all clients logged into the room (an array of type ClientInfo)
        ArrayList clientList;

        //The main socket on which the server listens to the clients
        Socket serverSocket;

        public event OnLoginHandler OnLogin;
        public event OnMessageReceivedHandler OnMessageReceived;
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        private static ManualResetEvent _sendDone = new ManualResetEvent(false);
        private static ManualResetEvent _receiveDone = new ManualResetEvent(false);

        private readonly int serverListenPort = 1000;

        public ControlChannelServer() {
            clientList = new ArrayList();
            try
            {
                //We are using TCP sockets
                serverSocket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream,
                    ProtocolType.Tcp);

                //Assign the any IP of the machine and listen on port number 1000
                IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, serverListenPort);

                //Bind and listen on the given address
                serverSocket.Bind(ipEndPoint);
                serverSocket.Listen(10);
                //while(true){
                // Set the event to nonsignaled state.
                //allDone.Reset();
                // Start an asynchronous socket to listen for connections.
                Console.WriteLine("Waiting for a connection...");
                //Accept the incoming clients
                serverSocket.BeginAccept(new AsyncCallback(OnAccept), serverSocket);
                //allDone.WaitOne();
            //}
        }
            catch (SocketException sex)
            {   
                Console.WriteLine(sex.Message,"Socket EX");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "ControlChannelServer");
            }
        }

        public void Logout(int connId)
        {
            int nIndex = 0;
            foreach(ClientInfo client in clientList)
            {
                if (client.connID == connId)
                {
                    if (client.socket.Connected)
                    {
                        SendTo(new Message(Message.MessageType.LOGOUT_OK), connId);
                        //_sendDone.WaitOne();
                        client.socket.Close();
                        //_sendDone.Set();
                    }
                    clientList.RemoveAt(nIndex); 
                }
                ++nIndex;
            }
        }

        private void OnAccept(IAsyncResult ar)
        {
            try
            {
                //allDone.Set();
                Socket listener = (Socket)ar.AsyncState;
                var state = new StateObject(listener.EndAccept(ar));
                serverSocket.BeginAccept(new AsyncCallback(OnAccept), serverSocket);
                //Once the client connects then start receiving the commands from her
                _sendDone.Set();
                _receiveDone.Set();
                state.workSocket.BeginReceive(state.buffer, 0, state.buffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), state);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "ControlCahnnelServer");
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                var state = (StateObject) ar.AsyncState;
                var clientSocket = state.workSocket;
                int bytesRead = clientSocket.EndReceive(ar);
                //_receiveDone.Set();

                if (bytesRead > 0)
                {
                    state.FinalBytes = StateObject.Combine(state.FinalBytes, state.buffer);
                }
                if (bytesRead == StateObject.BufferSize)
                {
                    state.ResetBuffer();
                    state.workSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(OnReceive), state);
                }
                else
                {
                    var bf = new BinaryFormatter();
                    var ms = new MemoryStream(state.FinalBytes);
                    var msgReceived = (Message) bf.Deserialize(ms);

                    if (msgReceived.Code == Message.MessageType.LOGIN_REQUEST)
                    {
                        ClientInfo clientInfo = new ClientInfo();
                        clientInfo.socket = clientSocket;
                        clientInfo.strName = msgReceived.MessageText;
                        clientInfo.connID = clientSocket.Handle.ToInt32();
                        clientList.Add(clientInfo);
                        //Set the text of the Message that we will broadcast to all users
                        OnLogin?.Invoke(msgReceived.MessageText, clientInfo.connID);
                    }
                    else if (msgReceived.Code == Message.MessageType.LOGOUT_REQUEST)
                        Logout(clientSocket.Handle.ToInt32());
                    //For Other Code Manage at Higher Level

                    //If the user is logging out then we need not listen from her
                    if (msgReceived.Code != Message.MessageType.CONNECTION_LOST ||
                        msgReceived.Code != Message.MessageType.LOGOUT_OK)
                    {
                        //_receiveDone.WaitOne();
                        //Start listening to the Message send by the 
                        var s = new StateObject(state.workSocket);
                        clientSocket.BeginReceive(s.buffer, 0, s.buffer.Length, SocketFlags.None,
                            new AsyncCallback(OnReceive), s);
                    }
                    OnMessageReceived?.Invoke(msgReceived, clientSocket.Handle.ToInt32());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "ControlCahnnelServer");
            }
        }

        public void OnSend(IAsyncResult ar)
        {
            try
            {
                StateObject s = (StateObject)ar.AsyncState;
                s.workSocket.EndSend(ar);
                _sendDone.Set();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "ControlCahnnelServer");
            }
        }

        public void SendToAll(Message m) {
            SendTo(m, -1);
        }
        public void SendToAllExcept(Message m, int connId)
        {
            SendTo(m,connId,true);
        }
        public void SendTo(Message m, int connId, bool allOthers = false)
        {
            var st = new StateObject();
            var mss = new MemoryStream();
            var bf = new BinaryFormatter();
            bf.Serialize(mss, m);
            byte[] b = mss.ToArray();
            //Console.WriteLine("BUFFER MESSAGE SERIALIZED SIZE = "+ b.Length);
            st.buffer = b;
            foreach (ClientInfo clientInfo in clientList)
            {
                if (connId == -1 || //toall
                    !allOthers && connId==clientInfo.connID || //to specific
                    allOthers && connId != clientInfo.connID) { //all other clients expept connI
                    st.workSocket = clientInfo.socket;
                    _sendDone.WaitOne();
                    clientInfo.socket.BeginSend(st.buffer, 0, st.buffer.Length, SocketFlags.None, new AsyncCallback(OnSend), st);
                }
            }
        }
    }
}
