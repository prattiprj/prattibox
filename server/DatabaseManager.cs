﻿using message_protocol;
using System;
using System.Collections;
using System.Data;
using System.Data.SQLite;
using System.IO;

namespace server
{
    class DatabaseManager
    {
        private const string db_name = "prattibox.sqlite";
        public string _DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        private string[] TABLE_COL_NAME = {"name", "lastwrite", "relativepath", "hash_digest", "deleted"};

        private const string create_table_string =
            "CREATE TABLE filelist (name TEXT NOT NULL, lastwrite TEXT NOT NULL, relativepath TEXT NOT NULL,hash_digest BLOB NOT NULL, deleted TEXT,PRIMARY KEY(hash_digest, relativepath))";

        private const string insert_file_record =
            "INSERT INTO filelist(name, lastwrite, relativepath, hash_digest, deleted) VALUES(@Name,@Lastwrite,@Relativepath,@Hash,@Deleted);";

        private const string update_file_record =
            "UPDATE filelist SET name = @Name, lastwrite = @Lastwrite, relativepath= @Relativepath, deleted = @Deleted WHERE hash_digest = @Hash ;";

        private const string retrieve_all_distinct_names =
            "SELECT * FROM filelist GROUP BY name, relativepath ORDER BY name, relativepath DESC;";

        private const string retrieve_all_distinct_names_2 = "SELECT name, relativepath, hash_digest, deleted, MAX(lastwrite)As 'lastversion'  FROM filelist  WHERE relativepath NOT IN (SELECT relativepath FROM filelist WHERE deleted IS NOT NULL ORDER BY deleted DESC, name) GROUP BY name, relativepath ORDER BY relativepath, name;";

        private const string retrieve_last_version_of =
            "SELECT hash_digest, name, deleted, MAX(lastwrite) AS 'lastversion' FROM filelist WHERE relativepath = @Relativepath GROUP BY name;";

        private const string retrieve_all_version_of =
            "SELECT * FROM filelist WHERE relativepath = @Relativepath AND deleted IS NULL ORDER BY lastwrite DESC";

        private const string retrieve_deleted =
            "SELECT * FROM filelist WHERE deleted IS NOT NULL ORDER BY deleted DESC, name;";

        private const string delete_file_record = "DELETE FROM filelist WHERE hash_digest = @Hash ;";

        private const string set_deleted_file =
            "UPDATE filelist SET deleted = @Deleted WHERE hash_digest = (SELECT hash_digest FROM (SELECT hash_digest, MAX(lastwrite) AS 'latestVersion' FROM filelist WHERE name = @Name GROUP BY name));";

        private const string shiva_query_for_each_file =
            "SELECT * FROM filelist WHERE relativepath = @Relativepath and hash_digest NOT IN (SELECT hash_digest FROM (SELECT* FROM filelist where relativepath = @Relativepath ORDER BY lastwrite DESC limit @N ))";

        private const string update_move_or_rename_folder = "UPDATE filelist SET relativepath = replace( relativepath, @Oldpath, @Newpath ) WHERE relativepath LIKE @Oldpathmatch";

        private SQLiteConnection _connDb = null;

        internal void OpenDb(string stageFolderPath)
        {
            string dbPath = Path.Combine(stageFolderPath, db_name);
            if (!System.IO.File.Exists(dbPath))
            {
                try
                {
                    SQLiteConnection.CreateFile(dbPath);
                    _connDb = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;");

                    _connDb.Open();
                    SQLiteCommand buildup = new SQLiteCommand(create_table_string, _connDb);
                    buildup.ExecuteNonQuery();
                    Console.WriteLine("A new filelist db was created in " + stageFolderPath + db_name);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message, "filelist db server connection error");
                    //TODO
                }
            }
            else
            {
                try
                {
                    _connDb = new SQLiteConnection("Data Source=" + dbPath + ";Version=3;");
                    _connDb.Open();
                    Console.WriteLine("filelist db is connected");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message, "filelist db server connection error");
                    //TODO
                }
            }
        }

        internal void CloseDb()
        {
            try
            {
                _connDb.Close();
                GC.Collect();
                Console.WriteLine("filelist db is disconnected");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "Error closing file list database file");
            }
        }

        internal int InsertFile(file_s gift)
        {
            using (SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = insert_file_record;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@Name", gift.filename);
                //cmd.Parameters.Add("@Lastwrite", DbType.DateTime).Value = gift.lastWrite;
                cmd.Parameters.AddWithValue("@Lastwrite", gift.lastWrite.ToString(_DateTimeFormat));
                cmd.Parameters.AddWithValue("@Relativepath", gift.relativepath);
                //cmd.Parameters.AddWithValue("@Hash", gift.hash);
                cmd.Parameters.Add("@Hash", DbType.Binary).Value = gift.hash;
                if (gift.deleted != DateTime.MaxValue)
                    cmd.Parameters.AddWithValue("@Deleted", gift.deleted.ToString(_DateTimeFormat));
                else
                    cmd.Parameters.AddWithValue("@Deleted", DBNull.Value);
                try
                {
                    return cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    Console.WriteLine(ex.Message, "The file" + gift.filename + "is already stored");
                    return 0;
                }
            }
        }

        internal int UpdateFile(file_s current, file_s fresh)
        {
            if (current.hash == fresh.hash)
            {
                using (SQLiteCommand cmd = new SQLiteCommand(_connDb))
                {
                    cmd.CommandText = update_file_record;
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@Name", fresh.filename);
                    cmd.Parameters.AddWithValue("@Lastwrite", fresh.lastWrite.ToString(_DateTimeFormat));
                    cmd.Parameters.AddWithValue("@Relativepath", fresh.relativepath);
                    cmd.Parameters.Add("@Hash", DbType.Binary).Value = current.hash;
                    if (fresh.deleted != DateTime.MaxValue)
                        cmd.Parameters.AddWithValue("@Deleted", fresh.deleted.ToString(_DateTimeFormat));
                    else cmd.Parameters.AddWithValue("@Deleted", DBNull.Value);
                    try
                    {
                        return cmd.ExecuteNonQuery();
                    }
                    catch (SQLiteException ex)
                    {
                        Console.WriteLine(ex.Message,
                            "Something wrong in updating record of file " + current.filename + "with  " + fresh.filename);
                        return 0;
                    }
                }
            }
            return 0;
        }

        internal int UpdateFile(byte[] hash, file_s fresh)
        {
            file_s recon = new file_s(fresh);
            recon.hash = hash;
            return UpdateFile(recon, fresh);
        }

        public int DeleteFile(file_s deletedFile)
        {
            //TODO doublcheck if filename or path is better
            using (SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = set_deleted_file;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@Name", deletedFile.filename);
                //cmd.Parameters.AddWithValue("@Relativepath", deletedFile.relativepath);
                //cmd.Parameters.Add("@Hash", DbType.Binary).Value = deletedFile.hash;
                if (deletedFile.deleted != DateTime.MaxValue)
                    cmd.Parameters.AddWithValue("@Deleted", deletedFile.deleted.ToString(_DateTimeFormat));
                else
                {
                    Console.WriteLine("Wrong deletion date/time");
                    return 0;
                }
                try
                {
                    return cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    Console.WriteLine(ex.Message,
                        "Something wrong in marking deleted record of file " + deletedFile.filename);
                    return 0;
                }
            }
        }

        public ArrayList GetDeletedFileList()
        {
            ArrayList fl = new ArrayList();
            using(SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = retrieve_deleted;
                cmd.Prepare();
                try
                {
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        fl.Add(new file_s(
                            reader["name"].ToString(), 
                            reader["relativepath"].ToString(), 
                            DateTime.ParseExact((string)reader["lastwrite"], _DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture),
                            DateTime.ParseExact((string)reader["deleted"], _DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture), 
                            (byte[]) reader["hash_digest"]));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return fl;
        }

        public ArrayList GetCurrentFileList()
        {
            ArrayList fl = new ArrayList();
            using(SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = retrieve_all_distinct_names_2;
                cmd.Prepare();
                try
                {
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DateTime d = DateTime.ParseExact((string) reader["lastversion"], _DateTimeFormat,
                                System.Globalization.CultureInfo.InvariantCulture);
                        
                        fl.Add(new file_s(
                            reader["name"].ToString(),
                            reader["relativepath"].ToString(),
                            d,
                            DateTime.MaxValue,
                            (byte[]) reader["hash_digest"]));
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return fl;
        }

        public file_s GetLatestVersionOf(file_s f)
        {
            file_s lf=null;
            using(SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = retrieve_last_version_of;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@Relativepath", f.relativepath);
                try
                {
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        DateTime dl, d = DateTime.ParseExact((string)reader["lastversion"], _DateTimeFormat,
                                System.Globalization.CultureInfo.InvariantCulture);
                        if (reader["deleted"] is System.DBNull)
                        {
                            dl = DateTime.MaxValue;
                        }
                        else
                        { 
                            dl = DateTime.ParseExact((string) reader["deleted"], _DateTimeFormat,
                                System.Globalization.CultureInfo.InvariantCulture);
                        }
                        lf = new file_s(
                            reader["name"].ToString(),
                            f.relativepath,
                            d,
                            dl,
                            (byte[])reader["hash_digest"]);
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return lf;
        }

        public ArrayList GetAllVersionOf(file_s f)
        {
            ArrayList fl = new ArrayList();
            using(SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = retrieve_all_version_of;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@Relativepath", f.relativepath);
                try
                {
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        DateTime d;
                        if (reader["deleted"] is System.DBNull)
                        {
                            d = DateTime.MaxValue;}
                        else
                        {
                            d = DateTime.ParseExact((string)reader["deleted"], _DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
                        }
                        fl.Add(new file_s(
                            reader["name"].ToString(),
                            reader["relativepath"].ToString(),
                            DateTime.ParseExact((string) reader["lastwrite"], _DateTimeFormat,
                                System.Globalization.CultureInfo.InvariantCulture),
                            d,
                            (byte[]) reader["hash_digest"]));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return fl;
        }

        public ArrayList ShivaQuery()
        {
            ArrayList fileList = new ArrayList();
            ArrayList shitList = new ArrayList();
            try
            {
                //get all distinct name of files stored 
                using (SQLiteCommand cmd = new SQLiteCommand(_connDb))
                {
                    cmd.CommandText = retrieve_all_distinct_names;
                    cmd.Prepare();
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                        fileList.Add(new file_s(reader["name"].ToString(), reader["relativepath"].ToString(),
                            DateTime.MaxValue, DateTime.MaxValue, null));
                }
                //for each one retrieve all the oldest ones records
                foreach (file_s fr in fileList)
                {
                    using (SQLiteCommand cmd = new SQLiteCommand(_connDb))
                    {
                        cmd.CommandText = shiva_query_for_each_file;
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@Relativepath", fr.relativepath);
                        cmd.Parameters.AddWithValue("@N", FileManager.FILE_HISTORY_SIZE);
                        SQLiteDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            DateTime d = DateTime.MaxValue;
                            if (reader["deleted"] is System.DBNull) { }
                            else
                            {
                                d = DateTime.ParseExact((string) reader["deleted"], _DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
                            }
                            shitList.Add(new file_s(reader["name"].ToString(), reader["relativepath"].ToString(),
                                DateTime.ParseExact((string) reader["lastwrite"], _DateTimeFormat,
                                    System.Globalization.CultureInfo.InvariantCulture), d,
                                (byte[]) reader["hash_digest"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return shitList;
        }

        public int DeleteFileRecord(file_s deleteFile)
        {
            using(SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = delete_file_record;
                cmd.Prepare();
                cmd.Parameters.Add("@Hash", DbType.Binary).Value = deleteFile.hash;
                try
                {
                    return cmd.ExecuteNonQuery();
                }
                catch(SQLiteException ex)
                {
                    Console.WriteLine(ex.Message,
                        "Something wrong in deleting record of file " + deleteFile.filename);
                    return 0;
                }
            }
        }

        public int OnFolderRename(string oldRelativePath, string newRelativePath)
        {
            using(SQLiteCommand cmd = new SQLiteCommand(_connDb))
            {
                cmd.CommandText = update_move_or_rename_folder;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@Oldpath", oldRelativePath);
                cmd.Parameters.AddWithValue("@Newpath", newRelativePath);
                cmd.Parameters.AddWithValue("@Oldpathmatch", string.Concat(oldRelativePath, "%"));

                try
                {
                    return cmd.ExecuteNonQuery();
                }
                catch(SQLiteException ex)
                {
                    Console.WriteLine(ex.Message, "Something wrong in moving folder records");
                    return 0;
                }
            }

            return 0;
        }
    }
}
