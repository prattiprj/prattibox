﻿using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using EnterpriseDT.Net.Ftp;
using message_protocol;
using Shouldly;

namespace server
{
    class FileManager
    {
        private string User;
        private int ConnectionCounter = 0;
        DatabaseManager fileDB = new DatabaseManager();
        FtpServerWrapper ftpServer=null;
        ControlChannelServer _srvCommander = new ControlChannelServer();
        private string rootFolderPath;
        private string ServerIpAddress = "127.0.0.1";
        private readonly string CURRENT_FOLDER = "\\Current\\";
        private readonly string STAGE_FOLDER = "\\Stage\\";
        private readonly string BUFFER_FOLDER = "\\Buffer\\";

        public static int FILE_HISTORY_SIZE { get; private set; } = 3;
        System.Timers.Timer shivaTimer = new System.Timers.Timer();
        private int SHIVA_TRIGGER_TIME = 1*60; //sec
        
        private DirectoryInfo root, current, stage, buffer;

        public FileManager()
        {
            shivaTimer.Elapsed += new ElapsedEventHandler(OnShivaTriggerEvent);
            shivaTimer.Interval = SHIVA_TRIGGER_TIME*1000;
            shivaTimer.Enabled = false;
            
            //Register Callbacks
            _srvCommander.OnLogin += Start;
            _srvCommander.OnMessageReceived += OnMessage;
            //Properties.Settings.Default["mainfolder"] = "";
            if ((string)Properties.Settings.Default["mainfolder"] == "")
            {
                System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
                if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    rootFolderPath = fbd.SelectedPath;
                    Properties.Settings.Default["mainfolder"] = rootFolderPath;
                    Properties.Settings.Default.Save();
                }
            }
            else
                rootFolderPath = Properties.Settings.Default["mainfolder"].ToString();
        }

        private void OnShivaTriggerEvent(object sender, ElapsedEventArgs e)
        {
                Task.Factory.StartNew(() =>
                {
                    shivaTimer.Stop();
                    Console.WriteLine("\n\n\tShiva is coming            _/[°_°]\\_\n\n");
                    int eated = Shiva();
                    shivaTimer.Start();
                    Console.WriteLine("\n\n\tShiva is Happy      \\____...(^_^)...____/\n\n" +
                                          "\t                           ...{0}...",eated);
                });
        }

        //CB for Received Messages
        private void OnMessage(Message m, int ConnID)
        {
            Message send;
            switch (m.Code)
            {
                case Message.MessageType.NULL:
                    break;
                case Message.MessageType.LOGIN_REQUEST:
                    Console.WriteLine("<<<LOG IN>> [" + m.MessageText + "] ");
                    break;
                case Message.MessageType.LOGOUT_REQUEST:
                    //_srvCommander.Logout(User);
                    Console.WriteLine("<<<LOG OUT>>> [" + m.MessageText + "] ");
                    this.Close();
                    break;
                case Message.MessageType.CONNECTION_LOST:
                    this.Close();
                    break;
                case Message.MessageType.OK:
                    break;
                case Message.MessageType.FILE_SEND_START:
                    Console.WriteLine("Start Uploading:" + m.fileInfo.filename);
                    break;
                case Message.MessageType.FILE_DELETE:
                    if (fileDB.DeleteFile(m.fileInfo)>0)
                    {
                        File.Delete(Path.Combine(current.FullName, m.fileInfo.relativepath));
                        Console.WriteLine("File Deleted: " + m.fileInfo.filename);
                        _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FILE_DELETED,m.fileInfo), ConnID);
                    }
                    break;
                case Message.MessageType.NEW_FILE:
                    Console.WriteLine("New File Created: " + m.fileInfo.filename);
                    ;
                    break;
                case Message.MessageType.FILE_SEND_ERROR:
                    Console.WriteLine("Error uploading " + m.fileInfo.filename);
                    break;
                case Message.MessageType.LIST:

                    break;
                case Message.MessageType.GENERIC:

                    break;
                case Message.MessageType.FILE_SEND_END:
                    int result = fileDB.InsertFile(m.fileInfo);
                    if (result > 0)
                    {
                        string destination = Path.Combine(current.FullName, m.fileInfo.relativepath);
                        Console.WriteLine("File Registered to database:\n>>>" + m.fileInfo.ToString(true));
                        string dirPath =Path.GetDirectoryName(destination).ToString();
                        if(!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        File.Copy(Path.Combine(buffer.FullName, m.fileInfo.filename), destination, true);
                        File.Move(Path.Combine(buffer.FullName, m.fileInfo.filename), Path.Combine(stage.FullName, m.fileInfo.getHashAsString()));
                        _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FILE_UPLOADED, m.fileInfo), ConnID);

                    }
                    else
                    {
                        Console.WriteLine("ERROR not registered into db:\n>>>" + m.fileInfo.ToString());
                        File.Delete(Path.Combine(buffer.FullName, m.fileInfo.filename));
                    }
                    break;
                case Message.MessageType.FTP_SERVER_UP:
                    Console.WriteLine("FTP_SERVER_UP");
                    break;
                case Message.MessageType.FTP_SERVER_DOWN:
                    break;
                case Message.MessageType.FILE_RENAMED:
                    fileDB.UpdateFile(m.fileInfo.hash, m.fileInfo);
                    File.Move(Path.Combine(current.FullName, m.MessageText), Path.Combine(current.FullName, m.fileInfo.filename));
                    _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FILE_RENAMED, m.fileInfo, m.MessageText), ConnID);
                    break;
                case Message.MessageType.FILE_MODIFY:
                    Console.WriteLine("File Modifyied: " + m.fileInfo.filename);
                    _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FILE_MODIFY, m.fileInfo), ConnID);
                    break;
                case Message.MessageType.FILE_MOVE:
                    if (fileDB.UpdateFile(m.fileInfo.hash, m.fileInfo) > 0)
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(m.fileInfo.relativepath)))
                        {
                            Directory.CreateDirectory(Path.Combine(current.FullName,
                                Path.GetDirectoryName(m.fileInfo.relativepath)));
                        }
                        File.Copy(Path.Combine(stage.FullName,m.fileInfo.getHashAsString()),Path.Combine(current.FullName, m.fileInfo.relativepath));
                        _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FILE_MOVE, m.fileInfo), ConnID);
                    }
                    break;
                case Message.MessageType.FOLDER_RENAMED:
                    
                    string oldNamePath = Path.Combine(current.FullName, m.MessageText);
                    if(Directory.Exists(oldNamePath))
                    {
                        Directory.Move(oldNamePath,Path.Combine(current.FullName,m.fileInfo.relativepath));
                        fileDB.OnFolderRename(shared_functions.GetRelativePath(oldNamePath,current.FullName),m.fileInfo.relativepath);
                    }
                    else
                    {
                        Directory.CreateDirectory(Path.Combine(current.FullName,m.fileInfo.relativepath));
                    }
                    _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FOLDER_RENAMED, m.fileInfo, m.MessageText), ConnID);

                    break;
                case Message.MessageType.GET_CURRENT_HASH_OF:
                    var f = fileDB.GetLatestVersionOf(m.fileInfo);
                    _srvCommander.SendTo(new Message(Message.MessageType.HASH_OF_FILE,f), ConnID);
                    break;
                case Message.MessageType.RETRIEVE_FILE_LIST:
                    send = new Message(Message.MessageType.FILE_LIST);
                    send.fileList = fileDB.GetCurrentFileList();
                    _srvCommander.SendTo(send,ConnID);
                    break;

                case Message.MessageType.RETRIEVE_DELETED_FILE_LIST:
                    send = new Message(Message.MessageType.DELETED_FILE_LIST);
                    send.fileList = fileDB.GetDeletedFileList();
                    _srvCommander.SendTo(send, ConnID);
                    break;
                case Message.MessageType.RETRIEVE_FILE_VERSIONS:
                    send = new Message(Message.MessageType.FILE_VERSIONS);
                    send.fileList = fileDB.GetAllVersionOf(m.fileInfo);
                    _srvCommander.SendTo(send, ConnID);
                    break;
                case Message.MessageType.FILE_RESTORED:
                    var fileR = m.fileInfo;
                    m.fileInfo.deleted = DateTime.MaxValue;
                    m.fileInfo.lastWrite = DateTime.Now;
                    fileDB.UpdateFile(m.fileInfo, fileR);
                    Console.WriteLine("File restored: "+ m.fileInfo.ToString(true));
                    if (!Directory.Exists(Path.GetDirectoryName(Path.Combine(current.FullName, fileR.relativepath))))
                        Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(current.FullName, fileR.relativepath)));
                    File.Copy(Path.Combine(stage.FullName,fileR.getHashAsString()),Path.Combine(current.FullName,fileR.relativepath));
                    _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FILE_RESTORED, m.fileInfo), ConnID);
                    break;
                case Message.MessageType.VERSION_CONTROL_OK:
                    var fileC = m.fileInfo;
                    m.fileInfo.deleted = DateTime.MaxValue;
                    m.fileInfo.lastWrite = DateTime.Now;
                    fileDB.UpdateFile(m.fileInfo, fileC);
                    File.Copy(Path.Combine(stage.FullName, fileC.getHashAsString()), Path.Combine(current.FullName, fileC.relativepath),true);
                    _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_FILE_VERSION_CONTROLLED, m.fileInfo), ConnID);
                    break;
                case Message.MessageType.SET_HISTORY_SIZE:
                    FILE_HISTORY_SIZE = int.Parse(m.MessageText);
                    _srvCommander.SendToAllExcept(new Message(Message.MessageType.MC_SET_HISTORY_SIZE, m.MessageText), ConnID);
                    break;
                default:
                    Console.WriteLine("Unknown message received:", m.MessageText);
                    break;
            }
        }

        public void Start(string User, int connId)
        {
            this.User = User;
            rootFolderPath += "\\";
            root = Directory.CreateDirectory(rootFolderPath + User);
            current = Directory.CreateDirectory(rootFolderPath + User + CURRENT_FOLDER);
            stage = Directory.CreateDirectory(rootFolderPath + User + STAGE_FOLDER);
            buffer = Directory.CreateDirectory(rootFolderPath + User + BUFFER_FOLDER);

            fileDB.OpenDb(stage.FullName);
            //if(ConnectionCounter==0)
            if(ftpServer==null) 
                ftpServer = new FtpServerWrapper(root.FullName, ServerIpAddress);
            _srvCommander.SendTo(new Message(Message.MessageType.LOGIN_OK, User), connId);

           // if(ConnectionCounter++ == 0)
            ftpServer.Start();
            _srvCommander.SendTo(new Message(Message.MessageType.FTP_SERVER_UP, User), connId);
            _srvCommander.SendTo(new Message(Message.MessageType.MC_SET_HISTORY_SIZE, FILE_HISTORY_SIZE.ToString()), connId);

            shivaTimer.Enabled = true;

        }

        public void Close()
        {
            try
            {
                if (--ConnectionCounter == 0)
                {
                    shivaTimer.Enabled = false;
                    ftpServer.Stop();
                    fileDB.CloseDb();
                    this.User = null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "Closing File Manager");
            }
        }

        //This function clean the eldest files
        private int Shiva()
        {
            ArrayList oldFileList = fileDB.ShivaQuery();
            foreach (file_s of in oldFileList)
            {
                try
                {
                    File.Delete(Path.Combine(stage.FullName, of.getHashAsString()));
                    fileDB.DeleteFileRecord(of);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return oldFileList.Count;
        }
  
    }
}
