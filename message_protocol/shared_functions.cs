﻿using System;
using System.Security.Cryptography;
using System.IO;



namespace message_protocol
{
    public class shared_functions
    {
        public static byte[] fileToHash(FileInfo f)
        {
            byte[] hashValue = null;
            try
            {
                SHA256 mySHA256 = SHA256Managed.Create();
                FileStream fileStream = f.Open(FileMode.Open);
                fileStream.Position = 0;
                // Compute the hash of the fileStream.
                hashValue = mySHA256.ComputeHash(fileStream);
                // Close the file.
                fileStream.Close();
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("Error: The directory specified could not be found.");
            }
            catch (IOException)
            {
                Console.WriteLine("Error: file {0} in the directory could not be accessed.", f.Name);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return hashValue;
        }

        public static void PrintByteArray(byte[] array)
        {
            int i;
            for (i = 0; i < array.Length; i++)
            {
                Console.Write(String.Format("{0:X2}", array[i]));
                //if ((i % 4) == 3) Console.Write("")
            }
        }

        public static string toStringByteArray(byte[] array)
        {
            string s = "";
            if(array != null)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    s += String.Format("{0:X2}", array[i]);
                }
            }
            return s;
        }

        public static string GetRelativePath(string filespec, string folder)
        {
            Uri pathUri = new Uri(filespec);
            // Folders must end in a slash
            if(!folder.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                folder += Path.DirectorySeparatorChar;
            }
            Uri folderUri = new Uri(folder);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('\\', Path.DirectorySeparatorChar));
        }
    }
}
