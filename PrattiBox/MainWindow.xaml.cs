﻿using message_protocol;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PrattiBox
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string folderPath, serverAddress;
        private readonly string USER_NAME = "Antares";
        //ClientConnManager serverManager = new ClientConnManager();

        private bool HistorySizeEditMcMask = true;
        HoundDog virgola;

        public MainWindow()
        {
            InitializeComponent();
            Console.SetOut(new ConsoleWriter(logBox));
            BulkSyncBtn.Dispatcher.BeginInvoke(new Action(() =>
            {
                BulkSyncBtn.IsEnabled = false;
            }));
            HistorySizeSlider.Dispatcher.BeginInvoke(new Action(() =>
            {
                HistorySizeSlider.IsEnabled = false;
            }));
            folderPath = Properties.Settings.Default["FolderMonitored"].ToString();
            serverAddress = Properties.Settings.Default["ServerAddress"].ToString();
            if (folderPath == "") Button_Folder(this,null);
            RestoreDeletedBtn.IsEnabled = GetSelectedVersionBtn.IsEnabled = false;
            //EnableDownloadSync.IsChecked = true;
            //serverManager.connectToServer();
        }

        private void OnWindowClosing(object sender, EventArgs e)
        {
            //TODO save persistent data preferences
        }

        private void Button_Folder(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog fd = new CommonOpenFileDialog();
            fd.IsFolderPicker = true;
            fd.Multiselect = false;
            fd.Title = "Select Folder to Monitor";
            if (fd.ShowDialog() == CommonFileDialogResult.Ok)
            {
                folderPath = fd.FileName;
                Properties.Settings.Default["FolderMonitored"] = folderPath;
                Properties.Settings.Default.Save();
                if (connectionToggle.IsChecked.Value)
                    connectionToggle_Click(this, null);
            }
        }

        private void logBox_TextChanged(object s,EventArgs e) { }

        private void DeletedFileListBtn_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                var fl = virgola.commander.GetLatestCurrentDeletedFileList();

                Console.WriteLine("----------- Current Server DELETED Files List -------------");
                foreach(var f in fl.Values)
                {
                    Console.WriteLine(f.relativepath + " [" + f.getHashAsStringReduced() + "]");
                }
                Console.WriteLine("----------------------------------------------------");

            });
        }

        private void CurrentFileListBtn_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                var fl = virgola.commander.GetLatestCurrentFileList();

                Console.WriteLine("----------- Current Server Files List -------------");
                foreach(var f in fl.Values)
                {
                    Console.WriteLine(f.relativepath + " [" + f.getHashAsStringReduced() + "]");
                }
                Console.WriteLine("----------------------------------------------------");

            });
        }

        private void BulkSyncBtn_Click(object sender, RoutedEventArgs e)
        {
            BulkSyncProgressBar.Value = 0;
            //BulkSyncBtn.Dispatcher.BeginInvoke(new Action(() => { BulkSyncBtn.IsEnabled = false; }));
            Console.WriteLine("Start syncing......");
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += (object es, DoWorkEventArgs dw) =>
            {
                BackgroundWorker work = es as BackgroundWorker;

                    if((work.CancellationPending == true))
                    {
                        dw.Cancel = true;
                    }
                    else
                    {
                        // Perform a time consuming operation
                        virgola.BulkSync(true, work);
                    }

                
            };
            worker.ProgressChanged+= (object ps, ProgressChangedEventArgs pgea) =>
            {
                BulkSyncProgressBar.Value = pgea.ProgressPercentage;
            };
            /*
           worker.RunWorkerCompleted += (object s, RunWorkerCompletedEventArgs en) =>
            {
                if ((en.Cancelled == true) || (!(en.Error == null)))
                {}
                else
                {
                    BulkSyncBtn.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        BulkSyncBtn.IsEnabled = true;
                    }));
                }
            };
            */        
    worker.RunWorkerAsync();
          
        }

        private class ConsoleWriter : TextWriter
        {
            TextBox textBox = null;

            public ConsoleWriter(TextBox output)
            {
                textBox = output;
            }

            public override void Write(char value)
            {
                base.Write(value);
                textBox.Dispatcher.BeginInvoke(new Action(() =>
                {

                    textBox.AppendText(value.ToString());
                    textBox.ScrollToEnd();
                }));
            }

            public override Encoding Encoding
            {
                get { return System.Text.Encoding.UTF8; }
            }
        }

        private void RefreshBtn_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                var fl = virgola.commander.GetLatestCurrentDeletedFileList();
                DeletedFileListView.Dispatcher.BeginInvoke(new Action(() =>
                {
                    DeletedFileListView.Items.Clear();
                    foreach (file_s f in fl.Values)
                    {
                        DeletedFileListView.Items.Add(f);
                    }
                }));
            });
        }

        private void RestoreDeletedBtn_Click(object sender, RoutedEventArgs e)
        {
           var toRestore = DeletedFileListView.SelectedItems;
            Task.Factory.StartNew(() =>
            {
                foreach (file_s fr in toRestore)
                {
                    virgola.RestoreDeleted(fr);
                }

                RefreshBtn_Click(this,null);
            });
        }

        private void DeletedFileListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sel = DeletedFileListView.SelectedItems;
            if (sel.Count == 0)
                RestoreDeletedBtn.IsEnabled = false;
            else
            {
                RestoreDeletedBtn.IsEnabled = true;
            }
        }

        private void GetSelectedVersionBtn_Click(object sender, RoutedEventArgs e)
        {
            var toDownload = VersionControlListView.SelectedItems;
            Task.Factory.StartNew(() =>
            {
                foreach(file_s fr in toDownload)
                {
                    virgola.GetVersion(fr);
                }
                VersionControlListView.Dispatcher.BeginInvoke(new Action(() =>
                {
                    VersionControlListView.Items.Clear();
                }));
            });
        }

        private void PickLocalFileBtn_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog fd = new CommonOpenFileDialog();
            fd.IsFolderPicker = false;
            fd.Multiselect = false;
            fd.Title = "Select a File for version control";
            if(fd.ShowDialog() == CommonFileDialogResult.Ok)
            {
                string f = fd.FileName;
                var fi = new FileInfo(f);
                Task.Factory.StartNew(() =>
                {
                    var versionsList = virgola.commander.GetAllVersionOf(new file_s(fi, folderPath));
                    VersionControlListView.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        VersionControlListView.Items.Clear();
                        foreach(file_s fv in versionsList)
                        {
                            VersionControlListView.Items.Add(fv);
                        }
                    }));
                });
            }
        }

        private void VersionControlListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sel = VersionControlListView.SelectedItems;
            if(sel.Count == 0)
                GetSelectedVersionBtn.IsEnabled = false;
            else
            {
                GetSelectedVersionBtn.IsEnabled = true;
            }
        }

        private void connectionToggle_Click(object sender, RoutedEventArgs e)
        {
            if((bool) connectionToggle.IsChecked && ValidateServerIp())
            {
                virgola = new HoundDog(folderPath, USER_NAME, serverAddress);
                virgola.ConnectToggle = connectionToggle;
                virgola.setHistorySizeCB_ += (size) =>
                {
                    HistorySizeEditMcMask = true;
                    HistorySizeSlider.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        HistorySizeSlider.Value = size;

                    }));
                };
                BulkSyncBtn.Dispatcher.BeginInvoke(new Action(() =>
                {
                    BulkSyncBtn.IsEnabled = true;
                }));
                HistorySizeSlider.Dispatcher.BeginInvoke(new Action(() =>
                {
                    HistorySizeSlider.IsEnabled = true;
                }));
                virgola.Connect(USER_NAME, (int)HistorySizeSlider.Value);

            }
            else
            {
                connectionToggle.IsChecked = false;
                BulkSyncBtn.Dispatcher.BeginInvoke(new Action(() =>
                {
                    BulkSyncBtn.IsEnabled = false;
                }));
                HistorySizeSlider.Dispatcher.BeginInvoke(new Action(() =>
                {
                    HistorySizeSlider.IsEnabled = false;
                }));
                virgola?.Disconnect();
                virgola = null;
            }
            //serverManager.closeServerConnection
        }

        private bool ValidateServerIp()
        {
            IPAddress ip;
            string newSA = ServerAddressTextBox.Text;
            bool valid = !string.IsNullOrEmpty(newSA) && IPAddress.TryParse(newSA, out ip);
            if (valid)
            {
                Properties.Settings.Default["ServerAddress"] = newSA;
                serverAddress = newSA;
                return true;
            }
            return false;
        }

        private void HistorySizeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
                int x = (int) HistorySizeSlider.Value;
            if (!HistorySizeEditMcMask)
            {
                Task.Factory.StartNew(() =>
                {
                    virgola.commander.SendCommand(new Message(Message.MessageType.SET_HISTORY_SIZE, x.ToString()));
                });
                Console.WriteLine("History size: " + x);
            }
            else
            {
                HistorySizeEditMcMask = false;
            }
        }
    }
        
}
