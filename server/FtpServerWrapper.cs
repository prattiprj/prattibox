﻿using FubarDev.FtpServer;
using FubarDev.FtpServer.AccountManagement;
using FubarDev.FtpServer.FileSystem.DotNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using AddressFamily = System.Net.Sockets.AddressFamily;

namespace server
{
    class FtpServerWrapper
    {
        FtpServer ftpServer;
        public FtpServerWrapper(string folderPath, string serverAddress)
        {
            // allow only anonymous logins
            var membershipProvider = new AnonymousMembershipProvider();
            // use %TEMP%/TestFtpServer as root folder
            var fsProvider = new DotNetFileSystemProvider(folderPath, false);
            // Initialize the FTP server
            serverAddress = GetAllLocalIPv4(NetworkInterfaceType.Wireless80211).FirstOrDefault();
            //LocalIPAddress().ToString();
            ftpServer = new FtpServer(fsProvider, membershipProvider, serverAddress);
            ftpServer.OperatingSystem = "Windows";
            Console.WriteLine("FTP server IP = {0}", serverAddress);

        }
        private string[] GetAllLocalIPv4(NetworkInterfaceType _type)
        {
            List<string> ipAddrList = new List<string>();
            foreach(NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if(item.NetworkInterfaceType == _type && item.OperationalStatus == OperationalStatus.Up)
                {
                    foreach(UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
                    {
                        if(ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddrList.Add(ip.Address.ToString());
                        }
                    }
                }
            }
            if (ipAddrList.Count == 0)
            {
                return GetAllLocalIPv4(NetworkInterfaceType.Ethernet);
            }
            return ipAddrList.ToArray();
        }
        private IPAddress LocalIPAddress()
        {
            if(!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                return null;
            }

            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            return host
                .AddressList
                .FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
        }
        public void Start()
        {
            // Start the FTP server
            ftpServer.Start();
            Console.WriteLine("The Ftp server is Up!");
        }

        public void Stop()
        {
            // Stop the FTP server
            ftpServer.Stop();
            Console.WriteLine("The FTP server is stopped");
        }
    }
}
