﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace message_protocol
{
    public class StateObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 2048;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        public byte[] FinalBytes;
        // Received data string.
        public StringBuilder sb = new StringBuilder();

        public StateObject(Socket clientSocket)
        {
            this.workSocket = clientSocket;
        }
        public static byte[] Combine(byte[] first, byte[] second)
        {
            if (first == null)
                return second;
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
        public StateObject()
        {
        }
        public void ResetBuffer()
        {
            buffer = new byte[BufferSize];
        }
    }

    [Serializable]
    public class Message
    {
        public enum MessageType : int
        {
            NULL = 0,
            CONNECTION_ESTABLISHED = 1,
            CONNECTION_LOST = 2,
            LOGIN_REQUEST = 4,
            LOGOUT_REQUEST = 5,
            LOGIN_OK = 6,
            LOGOUT_OK = 7,
            GET_CURRENT_HASH_OF = 8,
            HASH_OF_FILE = 9,
            VERSION_CONTROL_OK = 10,
            SET_HISTORY_SIZE= 11,
            FTP_SERVER_UP = 40,
            FTP_SERVER_DOWN = 41,
            RETRIEVE_FILE_LIST = 51,
            RETRIEVE_DELETED_FILE_LIST = 52,
            FILE_LIST = 53,
            DELETED_FILE_LIST = 54,
            RETRIEVE_FILE_VERSIONS = 55,
            FILE_VERSIONS = 56,
            OK = 100,
            MC_SET_HISTORY_SIZE = 150,
            MC_FILE_DELETED = 151,
            MC_FILE_RENAMED = 152,
            MC_FILE_UPLOADED = 153,
            MC_FILE_RESTORED = 154,
            MC_FILE_VERSION_CONTROLLED = 155,
            MC_FILE_MOVE = 156,
            MC_FILE_MODIFY = 157,
            MC_FOLDER_RENAMED = 158,
            FILE_SEND_START = 200,
            FILE_SEND_END = 201,
            FILE_DELETE = 202,
            FILE_RENAMED = 203,
            FILE_MODIFY = 204,
            FILE_MOVE = 205,
            FILE_RESTORED = 206,
            NEW_FILE = 209,
            FILE_SEND_ERROR = 210,
            FOLDER_RENAMED = 220,
            LIST = 254,
            GENERIC = 255,


        }

        private DateTime m_time;
        private string m_message;
        public file_s fileInfo { get; set; }
        public ArrayList fileList { get; set; }
        public MessageType Code { get; set; }

        public Message(MessageType T)
        {
            Code = T;
            m_time = CalcTimestamp();
            m_message = "";
        }

        public Message(MessageType T, file_s fileInfo)
        {
            this.fileInfo = fileInfo;
            Code = T;
            m_time = CalcTimestamp();
            m_message = "";
        }

        public Message(MessageType T, file_s fileInfo, string message)
        {
            this.fileInfo = fileInfo;
            Code = T;
            m_time = CalcTimestamp();
            m_message = message;
        }

        public Message(MessageType T, string message)
        {
            Code = T;
            m_time = CalcTimestamp();
            m_message = message;
        }

        public Message(MessageType T, DateTime time, string message)
        {
            Code = T;
            m_time = time;
            m_message = message;
        }

        public DateTime TimeStamp
        {
            get { return m_time; }
            set { m_time = value; }
        }

        public string MessageText
        {
            get { return m_message; }
            set { m_message = value; }
        }

        private DateTime CalcTimestamp()
        {
            return DateTime.Now;
        }

    }

    [Serializable()]
    public class file_s : ISerializable
    {
        private byte[] _hash;
        private DateTime _timestamp;
        private string _filename;
        private string _relativepath;
        private DateTime _deleted;
        public bool IsDir { get; set; } = false;

        public byte[] hash
        {
            get { return _hash; }
            set { _hash = value; }
        }

        public DateTime lastWrite
        {
            get { return _timestamp; }
            set { _timestamp = value; }
        }

        public string filename
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public string relativepath
        {
            get { return _relativepath; }
            set { _relativepath = value; }
        }

        public DateTime deleted
        {
            get { return _deleted; }
            set { _deleted = value; }
        }

        private readonly int REDUCED_LENGTH = 9;

        public file_s(string fileName, string relativePath, DateTime lastWrite, DateTime deleted, byte[] hash)
        {
            if (deleted == null)
                this.deleted = DateTime.MaxValue;
            else
                this.deleted = deleted;

            this.lastWrite = lastWrite;
            this.filename = fileName;
            this.hash = hash;
            this.relativepath = relativePath;
        }

        public file_s(file_s copy)
        {
            this.lastWrite = copy.lastWrite;
            this.filename = copy._filename;
            this.hash = copy.hash;
            this.deleted = copy.deleted;
            this.relativepath = copy.relativepath;
        }

        public file_s(FileInfo fullFileName, string basePath)
        {
            try
            {

           

            if (File.GetAttributes(fullFileName.FullName).HasFlag(FileAttributes.Directory))
            {
                IsDir = true;
                this.hash = null;
            }
            else
            {
                this.hash = shared_functions.fileToHash(fullFileName);

            }
            this.filename = fullFileName.Name;
            this.lastWrite = fullFileName.LastWriteTime;
            this.relativepath = shared_functions.GetRelativePath(fullFileName.FullName, basePath);
            this.deleted = DateTime.MaxValue;
        }
        catch(Exception)
            {
                //...
            }
        }
        public file_s(FileInfo fullFileName, string basePath, byte[] hash)
        {
            this.filename = fullFileName.Name;
            this.hash = hash;
            this.lastWrite = fullFileName.LastWriteTime;
            this.relativepath = shared_functions.GetRelativePath(fullFileName.FullName, basePath);
            this.deleted = DateTime.MaxValue;
        }
        public file_s(FileInfo fullFileName, string basePath, bool IsDir)
        {
            this.IsDir = IsDir;
            this.filename = fullFileName.Name;
            this.hash = IsDir?null:shared_functions.fileToHash(fullFileName);
            this.lastWrite = fullFileName.LastWriteTime;
            this.relativepath = shared_functions.GetRelativePath(fullFileName.FullName, basePath);
            this.deleted = DateTime.MaxValue;
        }

        public file_s(SerializationInfo info, StreamingContext ctxt)
        {
            hash = (byte[])info.GetValue("hash", typeof(byte[]));
            filename = (string)info.GetValue("filename", typeof(string));
            relativepath = (string)info.GetValue("relativepath", typeof(string));
            lastWrite = (DateTime)info.GetValue("lastwrite", typeof(DateTime));
            deleted = (DateTime)info.GetValue("deleted", typeof(DateTime));
            
        }
        
        
        public String getHashAsString()
        {
            return shared_functions.toStringByteArray(hash);
        }

        public override string ToString()
        {
            string d = deleted==DateTime.MaxValue?deleted.ToString() : "NO";
            return "" + filename + " created (" + lastWrite + ")" + "deleted ("+  d +")" + " SHA256["+getHashAsString()+"]";
        }

        public string ToString(bool ReducedHash = false)
        {
            string d = deleted == DateTime.MaxValue ? deleted.ToString() : "NO";
            return "" + filename + " created (" + lastWrite + ")" + "deleted (" + d + ")" + " SHA256[" + getHashAsStringReduced() + "]";
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("hash",hash);
            info.AddValue("filename", filename);
            info.AddValue("relativepath", relativepath);
            info.AddValue("lastwrite",lastWrite);
            info.AddValue("deleted",deleted);
        }

        public string getHashAsStringReduced()
        {
            return getHashAsString().Substring(0, REDUCED_LENGTH);
        }
    }
  
}
