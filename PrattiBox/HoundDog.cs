﻿using EnterpriseDT.Net.Ftp;
using message_protocol;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace PrattiBox
{
    class HoundDog
    {
        internal delegate void HistorySizeHandler(int size);

        public ControlChannelClient commander;
        private string SERVER_ADDRESS = "127.0.0.1";
        private readonly string remoteBufferFolder = "Buffer\\";
        private readonly string remoteCurrentFolder = "Current\\";
        private readonly string remoteStageFolder = "Stage\\";

        public event HistorySizeHandler setHistorySizeCB_;
        FileSystemWatcher _fsw;
        FTPConnection _ftp = new FTPConnection();
        public ToggleButton ConnectToggle { get; set; }
        private Hashtable fileWriteTimeHashtable = new Hashtable();
        private Dictionary<string, file_s> MoveDetectionDictionary = new Dictionary<string, file_s>();

        public HoundDog(string folderToMonitor, string username, string ServerAddress)
        {
            try
            {
                commander = new ControlChannelClient();
                _ftp.ServerAddress = SERVER_ADDRESS = ServerAddress;
                _ftp.UserName = "anonymous";
                _ftp.Password = "anonymous@domain.com";
                _ftp.AutoLogin = true;

                _fsw = new FileSystemWatcher(folderToMonitor);
                _fsw.EnableRaisingEvents = false;
                _fsw.IncludeSubdirectories = true;
                _fsw.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.CreationTime |
                                    NotifyFilters.FileName |
                                    NotifyFilters.DirectoryName;
                // Register Callbacks
                _fsw.Deleted += OnChanged;
                _fsw.Renamed += OnRenamed;
                _fsw.Changed += OnChanged;
                _ftp.Uploaded += OnUploaded;
                _ftp.Downloaded += OnDownloaded;

                commander.FTPConnectCB_ += _ftp.Connect;
                commander.FTPDisconnectCB_ += _ftp.Close;
                commander.buttonDisconnectCB_ += SetConnectButtonText;
                commander.MultiClientMessageCB_ += OnMultiClientMessage;
                //TODO bulk transfer early sync (false)
                Console.WriteLine("Started Monitoring folder: " + folderToMonitor);
            }
            catch (SocketException sex)
            {
                Disconnect();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        ~HoundDog()
        {
            Disconnect();
        }

        internal void Connect(string userName, int historySize)
        {
            if(ConnectToggle != null)
                ConnectToggle.Dispatcher.BeginInvoke(new Action(() =>
                {
                    ConnectToggle.IsChecked = true;

                }));
            //SetConnectButtonText("Disconnect");
            commander.Login(userName,SERVER_ADDRESS);
            commander.SendCommand(new Message(Message.MessageType.SET_HISTORY_SIZE, historySize.ToString()));
            _fsw.EnableRaisingEvents = true;
        }

        internal void Disconnect()
        {
            try
            {
                if (_ftp.IsConnected)
                    _ftp.Close();
                commander.Logout();
                
            }
            catch (Exception ftpEx)
            {
                Console.WriteLine(ftpEx.Message, "FTPex");
            }
            finally
            {
                _fsw.EnableRaisingEvents = false;
                try
                {
                    if (ConnectToggle != null)
                        ConnectToggle.Dispatcher.BeginInvoke(new Action(() =>
                        {
                            ConnectToggle.IsChecked = false;

                        }));
                }
                catch(Exception e) { }
                
                //SetConnectButtonText("Connect");
            }
        }

        internal void BulkSync(bool EnableDownload, BackgroundWorker work)
        {
            int Progress = 0;
            work.ReportProgress(Progress);
            var watch = System.Diagnostics.Stopwatch.StartNew();
            _fsw.EnableRaisingEvents = false;
            var UploadDiffList = new ArrayList();
            var DownloadDifflist = new ArrayList();
           // var DeleteDiffList = new ArrayList();
            var LocalFilesDictionary = new Dictionary<string, file_s>();
            //var localFL = new ArrayList();
            var remoteFL = commander.GetLatestCurrentFileList();
            //var remoteDFL = commander.GetLatestCurrentDeletedFileList();
            DirectoryInfo dir = new DirectoryInfo(_fsw.Path);
            FileInfo[] files = dir.GetFiles("*", SearchOption.AllDirectories);

            foreach (FileInfo fi in files)
            {
                var currentF = new file_s(fi, (string) _fsw.Path);
                LocalFilesDictionary.Add(currentF.getHashAsString()+currentF.relativepath, currentF);
                if(!remoteFL.ContainsKey(currentF.getHashAsString() + currentF.relativepath))
                    UploadDiffList.Add(currentF);
                //localFL.Add(currentF);
            }
            int nOp = UploadDiffList.Count;
            if (EnableDownload)
            {
                foreach (file_s remF in remoteFL.Values)
                {
                    if (!LocalFilesDictionary.ContainsKey(remF.getHashAsString()+remF.relativepath))
                    {
                        DownloadDifflist.Add(remF);
                    }
                }
                nOp += DownloadDifflist.Count;
                foreach (file_s toDownload in DownloadDifflist)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(Path.Combine(_fsw.Path, toDownload.relativepath))))
                        Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(_fsw.Path, toDownload.relativepath)));
                    _ftp.DownloadFile(Path.Combine(_fsw.Path, toDownload.relativepath),
                       Path.Combine(remoteStageFolder ,toDownload.getHashAsString()));
                    Progress += 100/nOp;
                    work.ReportProgress(Progress);
                }
            }

            foreach(file_s f in UploadDiffList)
            { 
                //commander.SendCommand(new Message(Message.MessageType.FILE_MODIFY, f));
                _ftp.UploadFile(Path.Combine(_fsw.Path, f.relativepath), remoteBufferFolder + f.filename);
                Progress += 100 / nOp;
                work.ReportProgress(Progress);
            }
            Progress = 100;
            work.ReportProgress(Progress);
            watch.Stop();
            var ts = watch.Elapsed;
            string elapsedTime = String.Format("{0:00}h:{1:00}m:{2:00}.{3:00}s", ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds/10);
            Console.WriteLine("\n\n\tFolder Is Synced, tooks {1} and {0} files uploaded - {2} downloaded",
                UploadDiffList.Count, elapsedTime, DownloadDifflist.Count);
            _fsw.EnableRaisingEvents = true;

        }

        private void OnUploaded(object sender, FTPFileTransferEventArgs e)
        {
            file_s fu = new file_s(new FileInfo(e.LocalPath), (string) _fsw.Path);
            Message endUp = new Message(Message.MessageType.FILE_SEND_END, fu);
            commander.SendCommand(endUp);
        }

        private void OnDownloaded(object sender, FTPFileTransferEventArgs e)
        {
            Console.WriteLine("Downloaded " + e.RemoteFile + " in " + e.LocalPath);
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            file_s fc = new file_s(new FileInfo(e.FullPath), _fsw.Path);
            Message createM = new Message(Message.MessageType.NEW_FILE, fc);
            commander.SendCommand(createM);
            _ftp.UploadFile(e.FullPath, remoteBufferFolder + fc.filename);
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            DateTime lastWrite = File.GetLastWriteTime(e.FullPath);
            if (true)/*(!fileWriteTimeHashtable.ContainsKey(e.FullPath) ||
                (fileWriteTimeHashtable.ContainsKey(e.FullPath) &&
                 !(lastWrite.Equals(fileWriteTimeHashtable[e.FullPath]))))*/
            {
                fileWriteTimeHashtable[e.FullPath] = lastWrite;

                if (File.GetAttributes(e.FullPath).HasFlag(FileAttributes.Directory))
                {
                    commander.SendCommand(new Message(Message.MessageType.FOLDER_RENAMED,
                        new file_s(null, shared_functions.GetRelativePath(e.FullPath, _fsw.Path), DateTime.Now,
                            DateTime.MaxValue, null), e.OldName));
                }
                else
                {
                    var fc = new file_s(new FileInfo(e.FullPath), _fsw.Path);
                    fc.lastWrite = DateTime.Now;
                    commander.SendCommand(new Message(Message.MessageType.FILE_RENAMED, fc, e.OldName));
                }
            }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            //TODO pulizia dizionario file rimossi
            FileInfo f = new FileInfo(e.FullPath);
            file_s fs = new file_s(f, _fsw.Path);
            Message M = new Message(Message.MessageType.NULL);
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Deleted:
                    var fc = new file_s(new FileInfo(e.FullPath), _fsw.Path, null);
                    fc.deleted = DateTime.Now;
                    fc.hash = commander.GetHashFromServer(fc);
                    try
                    {
                        MoveDetectionDictionary.Add(fc.getHashAsString(), fc);
                    }
                    catch (Exception)
                    {
                        MoveDetectionDictionary.Remove(fc.getHashAsString());
                        MoveDetectionDictionary.Add(fc.getHashAsString(), fc);
                    }

                    var deleteM = new Message(Message.MessageType.FILE_DELETE, fc);
                    commander.SendCommand(deleteM);
                    break;
                case WatcherChangeTypes.Changed:
                    try
                    {
                        DateTime lastWrite = File.GetLastWriteTime(e.FullPath);
                        if (!fileWriteTimeHashtable.ContainsKey(e.FullPath) ||
                            (fileWriteTimeHashtable.ContainsKey(e.FullPath) &&
                             !(lastWrite.Equals(fileWriteTimeHashtable[e.FullPath]))))
                        {
                            fileWriteTimeHashtable[e.FullPath] = lastWrite;
                           // var cF = new file_s(new FileInfo(e.FullPath), (string) _fsw.Path);
                            if (File.GetAttributes(e.FullPath).HasFlag(FileAttributes.Directory))
                            {
                                //Directory level modifyied
                                var dirCh = new DirectoryInfo(e.FullPath);
                                FileInfo[] files = dirCh.GetFiles("*", SearchOption.TopDirectoryOnly);
                                FileInfo[] filesRoot = new DirectoryInfo((string) _fsw.Path).GetFiles("*",
                                    SearchOption.TopDirectoryOnly);
                                ;
                                var ls = new List<FileInfo>(filesRoot);
                                ls.AddRange(files);
                                foreach (FileInfo fi in ls)
                                {
                                    var currentF = new file_s(fi, (string) _fsw.Path);
                                    if (MoveDetectionDictionary.ContainsKey(currentF.getHashAsString()))
                                    {
                                        //MOVE
                                        M = new Message(Message.MessageType.FILE_MOVE, currentF,
                                            MoveDetectionDictionary[currentF.getHashAsString()].relativepath);
                                        commander.SendCommand(M);
                                        MoveDetectionDictionary.Remove(currentF.getHashAsString());
                                    }
                                }
                                MoveDetectionDictionary = new Dictionary<string, file_s>();
                            }

                            else if (f.LastWriteTime != f.CreationTime) //OnModify
                            {
                                M = new Message(Message.MessageType.FILE_MODIFY, fs);
                                commander.SendCommand(M);
                                _ftp.UploadFile(e.FullPath, remoteBufferFolder + fs.filename);
                            }
                            else if (f.LastWriteTime == f.CreationTime)
                            {
                                M = new Message(Message.MessageType.NEW_FILE, fs);
                                commander.SendCommand(M);
                                _ftp.UploadFile(e.FullPath, remoteBufferFolder + fs.filename);
                            }

                            else
                            {
                                Console.WriteLine(fs.ToString());
                            }

                        }
                        else
                        {
                            fileWriteTimeHashtable.Clear();
                        }
                    }
                    catch(Exception) { }
                    break;
                default:
                    break;
            }

        }

        private void SetConnectButtonText(bool Value)
        {
            if (ConnectToggle != null)
                ConnectToggle.Dispatcher.BeginInvoke(new Action(() =>
                {
                    ConnectToggle.IsChecked = Value;
                }));
        }

        private void SetConnectButtonText()
        {
            SetConnectButtonText(false);
        }

        public void RestoreDeleted(file_s fr, bool MC = false)
        {
            if (!Directory.Exists(Path.GetDirectoryName(Path.Combine(_fsw.Path, fr.relativepath))))
                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(_fsw.Path, fr.relativepath)));
            _ftp.DownloadFile(Path.Combine(_fsw.Path, fr.relativepath), remoteStageFolder + fr.getHashAsString());
            if (!MC)
            {
                fileWriteTimeHashtable[Path.Combine(_fsw.Path, fr.relativepath)] =
                    File.GetLastWriteTime(Path.Combine(_fsw.Path, fr.relativepath));
                commander.SendCommand(new Message(Message.MessageType.FILE_RESTORED, fr));
            }
        }

        public void GetVersion(file_s fr, bool MC = false)
        {
            if (!Directory.Exists(Path.GetDirectoryName(Path.Combine(_fsw.Path, fr.relativepath))))
                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(_fsw.Path, fr.relativepath)));
            _ftp.DownloadFile(Path.Combine(_fsw.Path, fr.relativepath), Path.Combine(remoteStageFolder, fr.getHashAsString()));
           if(!MC) {
                fileWriteTimeHashtable[Path.Combine(_fsw.Path, fr.relativepath)] =
                    File.GetLastWriteTime(Path.Combine(_fsw.Path, fr.relativepath));
            commander.SendCommand(new Message(Message.MessageType.VERSION_CONTROL_OK, fr));
        }
    }

    public void OnMultiClientMessage(Message m)
        {
            _fsw.EnableRaisingEvents = false;
            switch (m.Code)
            {
                case Message.MessageType.MC_SET_HISTORY_SIZE:
                    setHistorySizeCB_(int.Parse(m.MessageText));
                    break;
                case Message.MessageType.MC_FILE_DELETED:
                    File.Delete(Path.Combine(_fsw.Path, m.fileInfo.relativepath));
                    break;
                case Message.MessageType.MC_FILE_RESTORED:
                    RestoreDeleted(m.fileInfo,true);
                    break;
                case Message.MessageType.MC_FILE_VERSION_CONTROLLED:
                case Message.MessageType.MC_FILE_UPLOADED:
                case Message.MessageType.MC_FILE_MODIFY:
                case Message.MessageType.MC_FILE_MOVE:
                    GetVersion(m.fileInfo,true);
                    break;
                case Message.MessageType.MC_FILE_RENAMED:
                    if(!Directory.Exists(Path.GetDirectoryName(Path.Combine(_fsw.Path, m.fileInfo.relativepath))))
                        Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(_fsw.Path, m.fileInfo.relativepath)));

                    File.Move(Path.Combine(_fsw.Path, m.MessageText), Path.Combine(_fsw.Path, m.fileInfo.filename));
                    break;
                case Message.MessageType.MC_FOLDER_RENAMED:
                    string oldNamePath = Path.Combine(_fsw.Path, m.MessageText);
                    if(Directory.Exists(oldNamePath))
                    {
                        Directory.Move(oldNamePath, Path.Combine(_fsw.Path, m.fileInfo.relativepath));
                    }
                    break;

            }
            _fsw.EnableRaisingEvents = true;

        }
    }

}
