﻿CREATE TABLE [dbo].[Table]
(
    [name] NVARCHAR(50) NOT NULL, 
    [timestamp] TIMESTAMP NOT NULL, 
    [hash_digest] BINARY(128) NOT NULL, 
    PRIMARY KEY ([hash_digest])
)
