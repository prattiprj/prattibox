﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using message_protocol;

namespace PrattiBox
{
    public delegate void FtpConnectCbHandler();

    public delegate void ButtonConnectCbHandler();

    public delegate ArrayList FileListHandler();

    public delegate void MultiClientMessageHandler(Message m);

    internal class ControlChannelClient
    {
        public Socket clientSocket; //The main client socket
        public string UserName { get; set; } //Name by which the user logs into the room

        // ManualResetEvent instances signal completion.
        private static ManualResetEvent _connectDone = new ManualResetEvent(false);
        private static ManualResetEvent _sendDone = new ManualResetEvent(false);
        private static ManualResetEvent _receiveDone = new ManualResetEvent(false);
        private static ManualResetEvent _freshFileListsManualResetEvent = new ManualResetEvent(false);
        private static ManualResetEvent _freshDeletedFileListsManualResetEvent = new ManualResetEvent(false);
        private static ManualResetEvent _getHashFromServerManualResetEvent = new ManualResetEvent(false);
        private static ManualResetEvent _getAllVersionOfManualResetEvent = new ManualResetEvent(false);

        private readonly BinaryFormatter bf = new BinaryFormatter();
        private string serverIP = "127.0.0.1";
        private readonly int serverPort = 1000;
        private Dictionary<string, file_s> _latestCurrentFileList,_latestCurrentDeletedFileList;
        private file_s _latestHashFromServer;
        private ArrayList _latestAllVersionOf;

        public bool IsLoggedIn { get; set; }
        public event FtpConnectCbHandler FTPConnectCB_ , FTPDisconnectCB_;
        public event ButtonConnectCbHandler buttonDisconnectCB_;
        public event MultiClientMessageHandler MultiClientMessageCB_;
        public ControlChannelClient()
        {
            _latestCurrentFileList = new Dictionary<string, file_s>();
            _latestCurrentDeletedFileList = new Dictionary<string, file_s>(); ;
            IsLoggedIn = false;
        }

        public bool Login(string username, string serverIP)
        {
            UserName = username;
            this.serverIP = serverIP;
            try
            {
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                var ipAddress = IPAddress.Parse(serverIP);
                //Server is listening on port 1000
                var ipEndPoint = new IPEndPoint(ipAddress, serverPort);
                //Connect to the server and login

                IAsyncResult result = clientSocket.BeginConnect(ipEndPoint, OnConnect, new StateObject(clientSocket));

                bool success = result.AsyncWaitHandle.WaitOne(3000, true);
                if (!success)
                {
                    // NOTE, MUST CLOSE THE SOCKET
                    clientSocket.Close();
                    throw new ApplicationException("Failed to connect server, Timeout expired");
                }
                _connectDone.WaitOne();
                _receiveDone.Set();
                Receive(clientSocket);
            }
            catch (ApplicationException aex)
            {
                Console.WriteLine(aex.Message);
                buttonDisconnectCB_();
            }
            catch (SocketException sex)
            {
                Console.WriteLine("Connection Fail");
                buttonDisconnectCB_();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        internal void Logout()
        {
            SendCommand(new Message(Message.MessageType.LOGOUT_REQUEST, UserName));
        }

        public Dictionary<string, file_s> GetLatestCurrentFileList()
        {
            SendCommand(new Message(Message.MessageType.RETRIEVE_FILE_LIST));
            _freshFileListsManualResetEvent.WaitOne();
            return _latestCurrentFileList;
        }

        public Dictionary<string, file_s> GetLatestCurrentDeletedFileList()
        {
            SendCommand(new Message(Message.MessageType.RETRIEVE_DELETED_FILE_LIST));
            _freshDeletedFileListsManualResetEvent.WaitOne();
            return _latestCurrentDeletedFileList;
        }

        public ArrayList GetAllVersionOf(file_s local)
        {
            SendCommand(new Message(Message.MessageType.RETRIEVE_FILE_VERSIONS, local));
            _getAllVersionOfManualResetEvent.WaitOne();
            return _latestAllVersionOf;
        }

        public byte[] GetHashFromServer(file_s f)
        {
            try
            {
           
            SendCommand(new Message(Message.MessageType.GET_CURRENT_HASH_OF,f));
            _getHashFromServerManualResetEvent.WaitOne();
                return _latestHashFromServer.hash;
            }
            catch(Exception)
            {
                return null;
            }
            return null;
        }

        public void SendCommand(Message m)
        {
            try
            {
                var state = new StateObject(clientSocket);
                //Fill the info for the message to be send
                var mss = new MemoryStream();
                LogSend(m);
                bf.Serialize(mss, m);
                state.buffer = mss.ToArray();
                //Send it to the server
                state.workSocket.BeginSend(state.buffer, 0, state.buffer.Length, SocketFlags.None, OnSend, state);
                _sendDone.WaitOne();
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to send message to the server. (" + UserName + ")");
            }
        }

        internal void Receive(Socket c)
        {
            try
            {
                _receiveDone.WaitOne();
                // Create the state object.
                var state = new StateObject(c);
                // Begin receiving the data from the remote device.
                state.workSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(OnReceive), state);
            }
            catch(Exception e)
            {
                //Console.WriteLine(e.ToString());
            }

        }
        
        private void LogSend(Message m)
        {
            switch (m.Code)
            {
                case Message.MessageType.NULL:
                    break;
                case Message.MessageType.CONNECTION_ESTABLISHED:
                    
                    break;
                case Message.MessageType.LOGOUT_REQUEST:
                    break;
                case Message.MessageType.CONNECTION_LOST:
                    break;
                case Message.MessageType.OK:
                    break;
                case Message.MessageType.FILE_SEND_START:
                    Console.WriteLine("Start Uploading:" + m.fileInfo.filename);
                    break;
                case Message.MessageType.FILE_SEND_END:
                    Console.WriteLine("File Uploaded Correctly: " + m.fileInfo.filename);
                    ;
                    break;
                case Message.MessageType.FILE_DELETE:
                    Console.WriteLine("File Deleted: " + m.fileInfo.filename);
                    break;
                case Message.MessageType.NEW_FILE:
                    Console.WriteLine("New File Create: " + m.fileInfo.filename);
                    ;
                    break;
                case Message.MessageType.FILE_SEND_ERROR:
                    Console.WriteLine("Error uploading " + m.fileInfo.filename);
                    break;
                case Message.MessageType.FTP_SERVER_UP:
                    Console.WriteLine("FTP SERVER UP");
                    break;
                case Message.MessageType.RETRIEVE_DELETED_FILE_LIST:
                    Console.WriteLine("Retrieve deleted files list");
                    break;
                case Message.MessageType.RETRIEVE_FILE_LIST:
                    Console.WriteLine("Retrieve current files list");
                    break;
                case Message.MessageType.LOGIN_REQUEST:
                    Console.WriteLine("Login attempt.....");
                    break;
                default:
                    break;
            }
        }

        private void OnConnect(IAsyncResult ar)
        {
            try
            {
                var s = (StateObject) ar.AsyncState;
                s.workSocket.EndConnect(ar);
                _connectDone.Set();
                //We are connected so we login into the server
                SendCommand(new Message(Message.MessageType.LOGIN_REQUEST, UserName));

            }
            catch (SocketException sex)
            {
                buttonDisconnectCB_();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            try
            {
                var s = (StateObject) ar.AsyncState;
                s.workSocket.EndSend(ar);
                _sendDone.Set();
            }
            catch (ObjectDisposedException)
            {
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "SGSclientTCP: " + UserName);
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                var state = (StateObject) ar.AsyncState;
                var serverSocket = state.workSocket;
                int bytesRead = serverSocket.EndReceive(ar);
                if (bytesRead > 0)
                {
                    state.FinalBytes = StateObject.Combine(state.FinalBytes, state.buffer); 
                }
                if(bytesRead == StateObject.BufferSize)
                {
                    state.ResetBuffer();
                    state.workSocket.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                   new AsyncCallback(OnReceive), state);
                }
                else
                {
                    var ms = new MemoryStream(state.FinalBytes);
                    var msgReceived = (Message)bf.Deserialize(ms);
                    _receiveDone.Set();
                    Receive(clientSocket);
                    ConsumeMessage(msgReceived);
                }

            }
            catch (SocketException sex)
            {
                Console.WriteLine("Connection dropped");
                buttonDisconnectCB_();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, "SGSclientTCP: " + UserName);
            }
        }

        void ConsumeMessage(Message msgReceived )
        {
            switch(msgReceived.Code)
            {
                case Message.MessageType.FTP_SERVER_UP:
                    if(IsLoggedIn)
                    {
                        Console.WriteLine("Start connecting to ftp server...");
                        FTPConnectCB_();
                        Console.WriteLine("ftp server connected");
                    }
                    break;
                case Message.MessageType.LOGIN_OK:
                    IsLoggedIn = true;
                    Console.WriteLine("Welcome back [" + UserName + "]");
                    break;

                case Message.MessageType.CONNECTION_LOST:
                    try
                    {
                        FTPDisconnectCB_();
                        IsLoggedIn = false;
                        Console.WriteLine("Good Bye [" + UserName + "]");
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    break;
                case Message.MessageType.HASH_OF_FILE:
                    _latestHashFromServer = msgReceived.fileInfo;
                    _getHashFromServerManualResetEvent.Set();
                    break;
                case Message.MessageType.FILE_LIST:
                    _latestCurrentFileList.Clear();
                    foreach(file_s fr in msgReceived.fileList)
                        _latestCurrentFileList.Add(fr.getHashAsString() + fr.relativepath, fr);
                    _freshFileListsManualResetEvent.Set();
                    break;
                case Message.MessageType.DELETED_FILE_LIST:
                    _latestCurrentDeletedFileList.Clear();
                    foreach(file_s fr in msgReceived.fileList)
                        _latestCurrentDeletedFileList.Add(fr.getHashAsString() + fr.relativepath, fr);
                    _freshDeletedFileListsManualResetEvent.Set();
                    break;
                case Message.MessageType.FILE_VERSIONS:
                    _latestAllVersionOf = msgReceived.fileList;
                    _getAllVersionOfManualResetEvent.Set();
                    break;
                case Message.MessageType.LOGOUT_OK:
                    try
                    {
                        Console.WriteLine("Good Bye [" + UserName + "]");
                        IsLoggedIn = false;
                        //SendCommand(new Message(Message.MessageType.LOGOUT_OK));
                        if(clientSocket.Connected)
                            clientSocket.Close();
                        FTPDisconnectCB_();
                    }
                    catch(Exception ex)
                    {
                        //Console.WriteLine(ex.Message);
                    }
                    break;
                default:
                    MultiClientMessageCB_?.Invoke(msgReceived);
                    break;
            }
        }
    }
}
