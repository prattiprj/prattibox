﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    partial class server_service : ServiceBase
    {
        private SqlConnection conn_db = null;
        public server_service()
        {
            InitializeComponent();
            this.ServiceName = "prattibox_srvr";
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
           
            conn_db = new SqlConnection("Data Source=ServerName"+
                                       "Initial Catalog=DatabaseName"+
                                       "Server=localhost/prattiboxdb.mdf;" + 
                                       "Trusted_Connection=yes;" + 
                                       "database=database; " + 
                                       "connection timeout=30");
            try{
                conn_db.Open();
                Console.WriteLine("conn open");
                conn_db.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message,"server connection error");
                //TODO
            }
        }

        protected override void OnStop()
        {
            // TODO: inserire qui il codice delle procedure di chiusura necessarie per arrestare il servizio.
        }
    }
}
